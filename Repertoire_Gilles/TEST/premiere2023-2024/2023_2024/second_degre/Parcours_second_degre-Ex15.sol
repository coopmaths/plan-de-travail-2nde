\begin{enumerate}
\item $f$ admet un maximum car ...
\item Tableau de variations de $f$ (à justifier) :
\begin{center}
\begin{tikzpicture}[scale=0.8]
  \tkzTabInit{$x$/1,$f(x)$/2}{$-\infty$,$\frac{3}{2}$,$+\infty$}
    \tkzTabVar{-/,+/$\frac{7}{4}$,-/$~$}
    %\tkzTabIma{1}{3}{2}{0}
\end{tikzpicture}
      \end{center}
\end{enumerate}
