    \begin{colenumerate}
    \item $S=\left\{\dfrac{2}{3}\right\}$
    \item $S=\{-1-\sqrt{7};-1+\sqrt{7}\}$
    \item $S=\{0;3\}$
    \item $S=\{-\sqrt{5};\sqrt{5}\}$
    \end{colenumerate}
   
