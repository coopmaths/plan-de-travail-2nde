Soit $A=\dfrac14\left[(a+b)^2-(a-b)^2\right]$.
\begin{enumerate}[label=\arabic*.]
\item  $A=5$.
\item  $A=6$
\item C'est vrai pour les valeurs précédentes, reste à le prouver pour toutes les valeurs avec le calcul littéral.
\end{enumerate}
