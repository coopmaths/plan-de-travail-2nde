\begin{enumerate}[label=\arabic*.]
\item  $D=(a+5)^2-(a-5)^2=20a$.
\item Avec $a=\nombre{10000}$, on obtient $D=\nombre{200000}$.
\end{enumerate}

  
