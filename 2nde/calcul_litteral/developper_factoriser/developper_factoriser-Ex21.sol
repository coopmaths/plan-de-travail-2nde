    Soit \(ABC\) un triangle rectangle en \(A\) tel que \(BC = x + 7\) et \(AC = 5\), où \(x\) désigne un nombre positif. Nous devons exprimer \(AB^2\) en fonction de \(x\) sous forme factorisée.

    Dans un triangle rectangle, le théorème de Pythagore s'énonce comme suit :

    \[
    BC^2 = AB^2 + AC^2
    \]

    Nous connaissons les longueurs \(BC\) et \(AC\), alors nous les substituons dans l'équation :

    \[
    (x + 7)^2 = AB^2 + 5^2
    \]

    Développons \( (x + 7)^2 \) :

    \[
    (x + 7)^2 = x^2 + 14x + 49
    \]

    Et \( 5^2 \) :

    \[
    5^2 = 25
    \]

    Substituons ces valeurs dans l'équation :

    \[
    x^2 + 14x + 49 = AB^2 + 25
    \]

    Isolons \( AB^2 \) :

    \[
    AB^2 = x^2 + 14x + 49 - 25
    \]

    Simplifions :

    \[
    AB^2 = x^2 + 14x + 24
    \]

    Pour exprimer \(AB^2\) sous forme factorisée, nous devons factoriser \(x^2 + 14x + 24\). Nous cherchons deux nombres dont le produit est \(24\) et la somme est \(14\). Ces nombres sont \(12\) et \(2\). Donc :

    \[
    x^2 + 14x + 24 = (x + 12)(x + 2)
    \]

    Ainsi, la forme factorisée de \(AB^2\) en fonction de \(x\) est :

    \[
    AB^2 = (x + 12)(x + 2)
    \]
