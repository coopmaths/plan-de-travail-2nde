\section*{Correction de l'exercice}

\noindent Voici deux programmes de calcul:

\medskip
\hspace*{-0.5cm}
\begin{minipage}{0.48\linewidth}
\begin{center}\textbf{PROGRAMME A}
\psset{unit=0.95cm}
\begin{pspicture}(0,0.5)(6,6)
%\psgrid
\rput(3,5){Choisir un nombre}\psframe(1.4,4.7)(4.6,5.2)\psline{->}(2,4.7)(1.6,3.3)
\rput(2,3){Multiplier par 4}\psframe(0.8,2.8)(3.3,3.3)\psline{->}(3,4.7)(4.,4.3)
\rput(4,4){Soustraire 2}\psframe(3,3.8)(5,4.3)\psline{->}(1.4,2.8)(2,1.2)
\rput(4,2){Élever au carré}\psframe(2.8,1.7)(5.2,2.2)\psline{->}(4,3.8)(4,2.2)
\rput(3,1){ Ajouter les deux nombres}\psframe(1,0.7)(5,1.2)\psline{->}(4,1.7)(3,1.2)
\end{pspicture}
\end{center}
\end{minipage}
\begin{minipage}{0.47\linewidth}
\begin{center}\textbf{PROGRAMME B}
\vspace{1.5cm}
\psset{unit=1cm}
\begin{pspicture}(6,4.2)
%\psgrid
\uput[r](1,3.5){$\bullet~~$Choisir un nombre}
\uput[r](1,2.5){$\bullet~~$Calculer son carré}
\uput[r](1,1.5){$\bullet~~$Ajouter 6 au résultat.}
\psframe(1,3.8)(5,1.2)
\end{pspicture}
\end{center}
\end{minipage}

\begin{enumerate}[label=\arabic*.]
\item
\begin{enumerate}[label=\arabic*.]
\item Montrer que, si l'on choisit le nombre $5$, le résultat du programme A est $29$.

\textbf{Solution :}

Choisissons le nombre \(5\).
\begin{align*}
&1. \quad 5 \times 4 = 20 \\
&2. \quad 5 - 2 = 3 \\
&3. \quad 3^2 = 9 \\
&4. \quad 20 + 9 = 29
\end{align*}



\item Quel est le résultat du programme B si on choisit le nombre 5 ?

\textbf{Solution :}

Choisissons le nombre \(5\).
\begin{align*}
&1. \quad 5^2 = 25 \\
&2. \quad 25 + 6 = 31
\end{align*}

Le résultat est donc \(31\).
\end{enumerate}

\item Si on nomme $x$ le nombre choisi, expliquer pourquoi le résultat du programme A peut s'écrire $x^2 + 4$.

\textbf{Solution :}

Nommons le nombre choisi \(x\).
\begin{align*}
1.& \quad x \times 4 = 4x \\
2.& \quad 4x +(x- 2)^2  \\
3.& \quad 4x +x^2-4x+4  \\
4.& \quad x^2 +  4 \\
\end{align*}
\item Quel est le résultat du programme B si l'on nomme $x$ le nombre choisi ?

\textbf{Solution :}

Nommons le nombre choisi \(x\).
\begin{align*}
&1. \quad (x-2)^2 = x^2 -4x+4\\
\end{align*}

Le résultat est donc $x^2 -4x+4$

\item Les affirmations suivantes sont-elles vraies ou fausses? Justifier les réponses et écrire les étapes des éventuels calculs :
\begin{enumerate}[label=\arabic*.]
\item \og Si l'on choisit le nombre $\dfrac{2}{3}$, le résultat du programme B est $\dfrac{58}{9}$. \fg

\textbf{Solution :}

Choisissons le nombre \(\dfrac{2}{3}\).
\begin{align*}
&1. \quad \left(\dfrac{2}{3}\right)^2 = \dfrac{4}{9} \\
&2. \quad \dfrac{4}{9} + 6 = \dfrac{4}{9} + \dfrac{54}{9} = \dfrac{58}{9}
\end{align*}

L'affirmation est donc vraie.

\item \og Si l'on choisit un nombre entier, le résultat du programme B est un nombre entier impair. \fg

\textbf{Solution :}
Prenons un contre exemple : $n=2$.\\


\begin{align*}
&1. \quad n^2 =4 \\
&2. \quad 4 + 6 =10 ~\text{est un entier pair} \\
\end{align*}



L'affirmation est donc fausse.

\item \og Le résultat du programme B est toujours un nombre positif. \fg

\textbf{Solution :}

Le résultat du programme B est $(x-2)^2$ . Un carré est toujours positif.\\

L'affirmation est donc vraie.

\item \og Pour un même nombre entier choisi, les résultats des programmes A et B sont ou bien tous les deux des entiers pairs, ou bien tous les deux des entiers impairs. \fg

\textbf{Solution :}

Pour un nombre entier \(x\), les résultats des programmes A et B sont respectivement \(x^2 - 4x +4\) et \(x^2 +4\).

si $x$ est pair, alors il existe un entier $p$ tel que $x=2p$\\
\(x^2 - 4x +4\=4p^2-8p+4=4(p^2-2p+1)=4(p-1)^2\) qui est pair>.\\
De même que \(x^2 +4=4p^2+4=4(p^2+1)\) .
les deux résultats sont donc pairs.\\
si $x$ est impair, alors il existe un entier $p$ tel que $x=2p+1$\\
\begin{align*}
x^2 - 4x +4&=(2p+1)^2-4(2p+1)+4\\
&=4p^2+4p+1-8p-4+4\\
&=4p^2-4p+1\\
&=4(p^2-p)+1
\end{align*}
 qui est impair>.\\
De même que \begin{align*}
x^2 +4&=(2p+1)^2+4\\
&=4p^2+4p+5\\
&=4(p^2-p+1)+1
\end{align*}
qui est impair>.\\
les deux résultats sont donc impairs.\\
$x$ a donc la même parité que les deux programmes.\\
L'affirmation est vraie

\end{enumerate}
\end{enumerate}





