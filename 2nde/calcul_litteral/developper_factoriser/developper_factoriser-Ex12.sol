\begin{enumerate}[itemsep=1em]
\item $=16x^2-8x+1=(4x)^2-2 \times 4x \times 1 + 1^2=(4x-1)^2$
\item $=\dfrac{4}{49}x^2-64=\left(\dfrac{2}{7}x\right)^2-8^2=\left(\dfrac{2}{7}x-8\right)\left(\dfrac{2}{7}x+8\right)$
\end{enumerate}
