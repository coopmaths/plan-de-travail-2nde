Soit \( f \) la fonction définie sur \(\mathbb{R}\) par :

\[
\hspace*{2cm} f(x) = 2x^2 - 4x - 30
\]

\begin{enumerate}[label=\arabic*.]
\item Montrer que \( f(x) = (2x + 6)(x - 5) \).
\item Montrer que \( f(x) = (2x + 2)(x - 3) - 24 \).
\end{enumerate}

\subsection*{1. Démonstration de \( f(x) = (2x + 6)(x - 5) \)}

Développons l'expression \( (2x + 6)(x - 5) \) pour vérifier qu'elle est égale à \( f(x) \) :

\[
(2x + 6)(x - 5) = 2x \cdot x + 2x \cdot (-5) + 6 \cdot x + 6 \cdot (-5)
\]
\[
= 2x^2 - 10x + 6x - 30
\]
\[
= 2x^2 - 4x - 30
\]

Nous constatons que cette expression est égale à \( f(x) \). Donc,

\[
f(x) = 2x^2 - 4x - 30 = (2x + 6)(x - 5)
\]

Ainsi, nous avons montré que :

\[
f(x) = (2x + 6)(x - 5)
\]

\subsection*{2. Démonstration de \( f(x) = (2x + 2)(x - 3) - 24 \)}

Commençons par développer l'expression \((2x + 2)(x - 3)\) :

\[
(2x + 2)(x - 3) = 2x(x - 3) + 2(x - 3)
\]
\[
= 2x^2 - 6x + 2x - 6
\]
\[
= 2x^2 - 4x - 6
\]

Maintenant, considérons \( (2x + 2)(x - 3) - 24 \) :

\[
(2x + 2)(x - 3) - 24 = 2x^2 - 4x - 6 - 24
\]
\[
= 2x^2 - 4x - 30
\]

Nous constatons que cette expression est équivalente à \( f(x) \). Donc,

\[
f(x) = (2x + 2)(x - 3) - 24
\]

Ainsi, nous avons montré que :

\[
f(x) = (2x + 2)(x - 3) - 24
\]

  
