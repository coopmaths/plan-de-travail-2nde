    On considère la fonction \( g \) définie sur \( \mathbb{R} \) par :

\[
g(x) = (2x-3)^2 - 6
\]

Montrons que pour tout réel \( x \),

\[
g(x) = (x-3)(4x+1) - (x-6)
\]

Commençons par développer \( g(x) \) :

\[
g(x) = (2x-3)^2 - 6
\]

Développons \( (2x-3)^2 \) :

\[
(2x-3)^2 = 4x^2 - 12x + 9
\]

Donc,

\[
g(x) = 4x^2 - 12x + 9 - 6 = 4x^2 - 12x + 3
\]

Ensuite, développons \( (x-3)(4x+1) - (x-6) \) :

Calculons \( (x-3)(4x+1) \) :

\[
(x-3)(4x+1) = x \cdot 4x + x \cdot 1 - 3 \cdot 4x - 3 \cdot 1
\]
\[
= 4x^2 + x - 12x - 3
\]
\[
= 4x^2 - 11x - 3
\]

Ensuite, considérons \( (x-3)(4x+1) - (x-6) \) :

\[
(x-3)(4x+1) - (x-6) = 4x^2 - 11x - 3 - x + 6
\]
\[
= 4x^2 - 12x + 3
\]



Ainsi, nous avons montré que pour tout réel \( x \),

\[
g(x) = (x-3)(4x+1) - (x-6)
\]

 
