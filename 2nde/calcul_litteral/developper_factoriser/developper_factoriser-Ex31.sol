\begin{enumerate}[label=\arabic*.]
\item $x^2 -y^2=ab$.
\item $\left(x-y\right)^2=a^2-4b$.
\end{enumerate}
