  \begin{enumerate}[itemsep=1em]
    \item $ =  (6x+7)(9x+8)$ \\$A = 54x^2+48x+63x+56$ \\$A =$ ${\color[HTML]{f15929}\boldsymbol{ 54x^2+111x+56 }}$
    \item $ =  (8x-8)(7x+7)$ \\$B = 56x^2+56x-56x-56$ \\$B =$ ${\color[HTML]{f15929}\boldsymbol{ 56x^2-56 }}$
  \end{enumerate}
