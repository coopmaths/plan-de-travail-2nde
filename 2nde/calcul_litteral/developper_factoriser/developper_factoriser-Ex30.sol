\begin{enumerate}[label=\arabic*.]
 \item  $h(x)=2x^2-10x+12$.
 \item $2(x-2)(x-3)=\ldots = h(x)$.
 \item
 \begin{enumerate}[label=\arabic*.]
  \item Factorisée. $h(2)=0$
  \item Forme de l'énoncé. $h\left( \dfrac{5}{ 2}\right)=-\dfrac{1}{2}$
  \item Développée. $h(-1)=24$
 \end{enumerate}
\end{enumerate}
