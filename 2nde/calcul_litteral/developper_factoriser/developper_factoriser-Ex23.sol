  Soit \( f \) la fonction définie sur \(\mathbb{R}\) par :

\[
\hspace*{2cm} f(x) = (1 - 2x)^2 - 9
\]

\begin{enumerate}[label={}]
    \item Montrer que \( f(x) = 4x^2 - 4x - 8 \).
    \item Montrer que \( f(x) = (4 - 2x)(-2 - 2x) \).
\end{enumerate}

\subsection*{1. Démonstration de \( f(x) = 4x^2 - 4x - 8 \)}

Commençons par développer l'expression de \( f(x) \) :

\[
f(x) = (1 - 2x)^2 - 9
\]

Développons \((1 - 2x)^2\) :

\[
(1 - 2x)^2 = 1 - 4x + 4x^2
\]

Donc,

\[
f(x) = 1 - 4x + 4x^2 - 9
\]

Simplifions l'expression :

\[
f(x) = 4x^2 - 4x + 1 - 9
\]
\[
f(x) = 4x^2 - 4x - 8
\]

Ainsi, nous avons montré que :

\[
f(x) = 4x^2 - 4x - 8
\]

\subsection*{2. Démonstration de \( f(x) = (4 - 2x)(-2 - 2x) \)}

Pour montrer cette égalité, nous devons factoriser l'expression obtenue précédemment \( f(x) = 4x^2 - 4x - 8 \).

Cherchons à factoriser \( 4x^2 - 4x - 8 \). Remarquons que :

\[
4x^2 - 4x - 8 = 4(x^2 - x - 2)
\]

Factorisons \( x^2 - x - 2 \) :

\[
x^2 - x - 2 = (x - 2)(x + 1)
\]

Donc,

\[
4(x^2 - x - 2) = 4(x - 2)(x + 1)
\]

Cependant, nous devons exprimer \( f(x) \) sous la forme \( (4 - 2x)(-2 - 2x) \).

Considérons l'expression :

\[
(4 - 2x)(-2 - 2x)
\]

Développons cette expression :

\[
(4 - 2x)(-2 - 2x) = 4 \cdot (-2) + 4 \cdot (-2x) - 2x \cdot (-2) - 2x \cdot (-2x)
\]
\[
= -8 - 8x + 4x + 4x^2
\]
\[
= 4x^2 - 4x - 8
\]

Nous constatons que cette expression est équivalente à \( f(x) \). Donc,

\[
f(x) = (4 - 2x)(-2 - 2x)
\]

Ainsi, nous avons montré que :

\[
f(x) = (4 - 2x)(-2 - 2x)
\]


  
