\documentclass[11pt,french]{article}
\usepackage{ProfCollege}
\usepackage{ProfMaquette}

% Modifictaion pour la compilation en mode luaLaTeX
% Font
\usepackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}

% Pour gérer la géométrie de la page.
\usepackage[a4paper,margin=1cm,noheadfoot]{geometry}
%\usepackage[parfill]{parskip} % ligne entre les paragraphes
\usepackage{setspace} % gestion des interlignes
\thispagestyle{empty}
% Pour utiliser les usages français grâce au <french> de l'option de classe.
\usepackage{babel}

\begin{document}
\begin{Maquette}[IE,CorrigeFin]{Numero=1, Niveau=Seconde, Theme=Vérifier la mémoire au retour des vacances,Nom=Diagnostic }
% -------------- Exo 1 ----------------
\begin{exercice}[Titre=Préciser si les affirmations sont vraies ou fausses]
\QCM[Multiple,Alterne,Reponses=3,Stretch=3,Largeur=2.5cm,%
Noms={Vrai/Faux/Je ne sais pas}]{%
$3+5\times2$ et $16$ sont égaux.&0&1&0,%
$5\times78$ et $5\times80-5\times2$ sont égaux.&1&0&0,%
$\num{-6,8}$ est supérieur à $\num{-6,3}$.&0&1&0,%
Multiplier par $\dfrac{1}{8}$ revient à diviser par $8$.&1&0&0,%
$\dfrac{30}{50}$ et $\dfrac{3}{5}$ sont égaux.&1&0&0,%
$\dfrac{23}{53}$ et $\dfrac{2}{5}$ sont égaux.&0&1&0,%
$\dfrac{6}{15}$ et $\dfrac{2}{5}$ sont égaux.&1&0&0,%
$\dfrac{23}{41}$ est la nombre qui multiplié par $41$ donne $23$.&1&0&0,%
$a^2=2a$ est vraie pour toutes les valeurs de $a$.&0&1&0,%
$(a+1)^2=a^2+1$ est vraie pour toutes les valeurs de $a$.&0&1&0
}
\end{exercice}
% Correction
\begin{Solution}
\QCM[Multiple,Alterne,Solution,Reponses=3,Stretch=2,Largeur=2.5cm,%
Noms={Vrai/Faux/Je ne sais pas}]{%
$3+5\times2$ et $16$ sont égaux.&0&1&0,%
$5\times78$ et $5\times80-5\times2$ sont égaux.&1&0&0,%
$\num{-6,8}$ est supérieur à $\num{-6,3}$.&0&1&0,%
Multiplier par $\dfrac{1}{8}$ revient à diviser par $8$.&1&0&0,%
$\dfrac{30}{50}$ et $\dfrac{3}{5}$ sont égaux.&1&0&0,%
$\dfrac{23}{53}$ et $\dfrac{2}{5}$ sont égaux.&0&1&0,%
$\dfrac{6}{15}$ et $\dfrac{2}{5}$ sont égaux.&1&0&0,%
$\dfrac{23}{41}$ est la nombre qui multiplié par $41$ donne $23$.&1&0&0,%
$a^2=2a$ est vraie pour toutes les valeurs de $a$.&0&1&0,%
$(a+1)^2=a^2+1$ est vraie pour toutes les valeurs de $a$.&0&1&0
}\\
\end{Solution}

% -------------- Exo 2 ----------------
\begin{exercice}[Titre=Souligner la(les) bonne(s) réponse(s) :]
\QCM[Depart=11,Alterne,Reponses=4,Stretch=3,Titre,AlphT]{%
$2-7=$\dots & $-9$ & $-5$ & $5$ & $9$ &2,%
$-3-(-10)=$\dots & $7$ & $-7$ & $13$ & $-13$ &1,%
$\dfrac{2}{3}+\dfrac{5}{3}=$\dots & $\dfrac{7}{6}$ & $\dfrac{7}{3}$ & $\dfrac{10}{3}$ & $\dfrac{10}{9}$ &2,%
$3\times49+3\times5=$\dots & $3\times(49+5)$ & $6\times(49+5)$ & $9\times(49+5)$ & $3\times49+5$ &1,%
$7\times15+21=$\dots & $7\times36$ & $7\times(15+3)$ & $7\times(15+21)$ & $3\times(35+7)$ &2,%
La forme simplifiée de $3\times a\times 5-2+a$ est\dots & $3a5-2+a$ & $3a5-2a$ & $12a$ & $14a-2$ &4,%
Le produit de la somme de $9$ et $6$ par la somme de $4$ et $5$ s'écrit\dots & $(9+6)(4+5)$ & $9\times6+4\times5$ & $9+6\times4+5$ & $(9+6+4+5)^2$ &1,%
$2\times(x+5)=$\dots & $2x+10$ & $x+10$ & $x+7$ & $2x+5$ &1,%
$x(x+5)=$\dots & $2x+5x$ & $x+5x$ & $x^2+5$ & $x^2+5x$ &4,%
$2x\left(x-\dfrac{3}{4}\right)=$\dots & $3x-\dfrac{3}{4}$ & $3x-\dfrac{3}{2}$ & $2x^2-\dfrac{3}{2}x$ & $2x^2-\dfrac{3x}{4}$ &3
}
\end{exercice}
% Correction
\begin{Solution}
\QCM[Depart=11,Alterne,Reponses=4,Stretch=2,Titre,AlphT,Solution,Couleur=yellow!15]{%
$2-7=$\dots & $-9$ & $-5$ & $5$ & $9$ &2,%
$-3-(-10)=$\dots & $7$ & $-7$ & $13$ & $-13$ &1,%
$\dfrac{2}{3}+\dfrac{5}{3}=$\dots & $\dfrac{7}{6}$ & $\dfrac{7}{3}$ & $\dfrac{10}{3}$ & $\dfrac{10}{9}$ &2,%
$3\times49+3\times5=$\dots & $3\times(49+5)$ & $6\times(49+5)$ & $9\times(49+5)$ & $3\times49+5$ &1,%
$7\times15+21=$\dots & $7\times36$ & $7\times(15+3)$ & $7\times(15+21)$ & $3\times(35+7)$ &2,%
La forme simplifiée de $3\times a\times 5-2+a$ est\dots & $3a5-2+a$ & $3a5-2a$ & $12a$ & $14a-2$ &4,%
Le produit de la somme de $9$ et $6$ par la somme de $4$ et $5$ s'écrit\dots & $(9+6)(4+5)$ & $9\times6+4\times5$ & $9+6\times4+5$ & $(9+6+4+5)^2$ &1,%
$2\times(x+5)=$\dots & $2x+10$ & $x+10$ & $x+7$ & $2x+5$ &1,%
$x(x+5)=$\dots & $2x+5x$ & $x+5x$ & $x^2+5$ & $x^2+5x$ &4,%
$2x\left(x-\dfrac{3}{4}\right)=$\dots & $3x-\dfrac{3}{4}$ & $3x-\dfrac{3}{2}$ & $2x^2-\dfrac{3}{2}x$ & $2x^2-\dfrac{3x}{4}$ &3
}
\end{Solution}
​
\end{Maquette}
\end{document}

\colorcell{yellow!15}
$(a+2)(a-5)=$\dots & $2a-5a+2a-10$ & $2a^2-3a-10$ & $2a^2-3a+10$ & $a^2-10$ &2,%
$a^n=$\dots & $a+a+$\dot$+a$ avec $n$ termes & $a\times n$ & $a\times a\times$\dot$\times a$ avec $n$ termes & $\dfrac{a}{-n}$ &3
