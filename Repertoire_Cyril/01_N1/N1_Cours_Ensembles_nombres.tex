% Pour gérer pstricks avec Lualatex
\DocumentMetadata{}
\documentclass[11pt]{article}
\setlength{\columnsep}{50pt}

\usepackage{ProfMaquette}

% Maths
\usepackage{mathtools}
\usepackage{amssymb}

\usepackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}
\usepackage[math-style=french,Scale=0.98]{fourier-otf}

% Chiffres romains
\usepackage{romanbar}

\usepackage[a4paper,margin=1cm,noheadfoot]{geometry}
\setlength{\parindent}{0pt}
\pagestyle{empty}

%Pour mieux gérer les listes
\usepackage{enumitem}
% Je laisse la définition que tu as créée mais ce n'est pas la typographie française...
\setlist[itemize]{leftmargin=10pt,label=\textbullet}

\usepackage[luatex]{hyperref}
\hypersetup{
    colorlinks=true,
    urlcolor=red!75!green!50,
    linkcolor=red!75!green!50,
    breaklinks=true
}

% Images et Dessins
\usepackage{graphicx}
\usepackage{tikz} 
\usepackage{pstricks-add}

\usepackage[french]{babel}
%---------------------------------------------------------
\begin{document}

\begin{Maquette}[Fiche]{Theme= Qu'est-ce qu'un nombre?,Classe={},Niveau=2de, Date={}}


\section{Des obstacles à l'apprentissage du concept de nombre}
La notion apparemment familière de nombre ne va pas de soi. Il existe plusieurs raisons à cela, selon David Bessis\footnote{\href{https://www.seuil.com/ouvrage/mathematica-david-bessis/9782021493979}{Mathematica}, Une aventure au cœur de nous-mêmes, David Bessis, 2022, Edition Seuil}:

\subsection{Imaginer ce qui n'existe pas}

Imagine un cercle, parfaitement rond, sans aucun défaut. Le vois-tu ? Pourtant dans la vraie vie, un tel cercle n'existe pas ! Rien n'est jamais parfaitement rond, ni les roues de vélo, ni le disque du soleil, etc. Pourtant, tu peux le voir, le déplacer, l'agrandir, le rétrécir mentalement, etc. Il en est de même pour les nombres : personne n'a jamais vu deux, nous pouvons en voir une présentation graphique, ou imaginer une \og quantité de \ldots \fg{}. C'est l'un de vos pouvoirs magiques, appelé \textbf{abstraction}.\par
Prends \textit{un milliard}. Enlève \textit{1}. Combien reste-t-il ? Pas besoin de réfléchir bien longtemps, nous pouvons voir le résultat dans notre tête : \textit{\num{999999999}}. Il est même plus facile à voir qu'à prononcer. Ça nous semble évident et, pourtant, ça ne l'est pas pour tout le monde. Pour un habitant de la Rome antique, ça n'était pas évident du tout : en latin classique, le mot \textit{milliard} n'existe pas. Aussi, essaye d'écrire \textit{\num{999999999}} en chiffre romain et tu comprendras pourquoi une personne de l'époque de Jules César te prendra pour un surdoué en maths.\par 
L'écriture décimale qui nous donne l'impression que ce nombre est physiquement présent sur cette page n'est que le résumé d'une longue définition formelle, qui caractérise ce nombre comme le résultat d'enchaînement d'additions et multiplications :\textit{neuf plus neuf fois dix, plus neuf fois dix fois dix, plus neuf fois dix fois dix fois dix, plus neuf fois dix fois dix fois dix fois dix, plus neuf fois dix fois dix fois dix fois dix fois dix, plus neuf fois dix fois dix fois dix fois dix fois dix fois dix, plus neuf fois dix fois dix fois dix fois dix fois dix fois dix fois dix, plus neuf fois dix fois dix fois dix fois dix fois dix fois dix fois dix fois dix}. Sur le papier, ce nombre est un assemblage abstrait, logique et froid. Alors que dans notre tête, c'est un objet simple concret et évident.\par
Pour comprendre ce que l'on ne comprend pas en mathématiques, il faut effectuer dans sa tête des gestes silencieux, invisibles mais indispensables, qui permettront d'enrichir son intuition et de développer de nouvelles images mentales. Au départ imparfaites, nous les faisons évoluer au fur et à mesure des erreurs que nous corrigeons. Ce n'est pas inné, mais nous tous avons la faculté d'apprendre à le faire. Je vais vous y aider, alors à vos questions !

\begin{minipage}{12cm}
\subsection{Le langage mathématique est différent du langage humain}
Le dictionnaire est rempli de définitions circulaires. Qu'est-ce que la chaleur ? Qualité de ce qui est chaud. Que veut dire chaud ? De température plus haute que la normale. Qu'est-ce qu'une température ? Degré de chaleur, ou de froideur, d'un corps ou d'un environnement.
\end{minipage}
\begin{minipage}{6cm}
\begin{center}
% \fbox{
\begin{tikzpicture}
  \node[] (A) at (0,0){Température};
  \node[] (B) at (4,0){Chaud};
  \node[] (C) at (2,2){Chaleur};
  \tikzstyle{fleche}=[->,very thick,>=latex]
  \draw[fleche] (A) to[bend left] (C);
  \draw[fleche] (C) to[bend left] (B);
  \draw[fleche] (B) to[bend left] (A);
\end{tikzpicture}
% } % fin boîte
\end{center}
\end{minipage}

Contrairement au dictionnaire, les textes mathématiques ne se contentent pas de relier entre eux des mots qui existent déjà. Une définition mathématique est le guide d'assemblage précis d'une nouvelle image mentale, qui s'appuie sur des mots définis antérieurement, sans effet circulaire.
Une difficulté est que très souvent nous utilisons déjà des mots du langage humain, qui peuvent avoir des significations différentes suivant le contexte. Nous parlons de \textit{polysémie} des mots, comme le mot \textit{rayon}, dont les définitions suivantes sont données par le dictionnaire \textit{Larousse}:
\begin{itemize}
   \item Trait, ligne qui part d'un centre lumineux : \textit{Un rayon de soleil}.
   \item Distance déterminée à partir d'un centre, d'un point d'origine dans toutes les directions : \textit{Dans un rayon de trente kilomètres autour de Paris}.
   \item Tige métallique de très faible diamètre reliant à la jante le moyeu d'une roue.
   \item Anatomie : Chacune des pièces squelettiques qui soutiennent les nageoires des poissons.
   \item Hippologie : Ensemble des os de la partie supérieure des membres du cheval, dont la longueur et l'orientation influent sur la qualité des allures.
   \item Mathématiques : Segment dont une extrémité est le centre d'un cercle, d'une sphère, d'un disque, d'une boule, l'autre étant un point du cercle, de la sphère ou de la frontière du disque ou de la boule ; longueur de ce segment.
\end{itemize}

Un autre obstacle est l'abus de langage courant. Par exemple, lorsque nous parlons des \textit{chiffres du chômage}, alors qu'en mathématiques, nous savons qu'il s'agit bien d'un nombre, composé de chiffres, comme un mot est composé de lettres.

\section{La construction du nombre a une longue histoire}
Plusieurs animaux, comme les abeilles, les rats, les pigeons, les singes, l'Homme, etc. ont la conscience du nombre et du calcul. Pour autant, certains peuples n'ont pas de mots au-delà de deux. Il semble donc qu'il faille des conditions particulières pour que la notion de nombre se développe. Les traces préhistoriques d'activités mathématiques restent discutables (cf. bâtons d'Ishango, il y a environ 20 000 ans), il faut attendre l'apparition de l'écriture il y a environ \num{5000} ans en Mésopotamie (Irak actuel) pour avoir des traces d'activités arithmétiques (branche des mathématiques qui étudie les propriétés et les règles de calcul entre les nombres).\par
Quand on voit ci-dessous quelques dates remarquables sur le construction du concept de nombre, on se rend compte du temps qu'il a fallu à nos ancêtres pour manipuler cette notion. Pas étonnant que nous rencontrions quelques difficultés par moment, alors que nous abordons ces concepts en quelques heures à l'école. Sois donc indulgent avec toi-même, prends le temps d'apprendre.\par

\begin{itemize}
\item Il y a plus de \num{3000} ans avant JC, les Égyptiens de l'Antiquité utilisaient un système de numération décimal, mais dans lequel zéro n'existait pas. Chaque ordre de grandeur (unités, dizaines, centaines, etc.) possédait un signe répété le nombre de fois nécessaire. Autrement dit, il s'agit d'un système additif et non pas d'un système de position. À la même période, les mésopotamiens utilisaient un système de numérotation positionnelle en base 60. Il en reste des traces dans notre système horaire ou dans la mesure des angles en degrés, minutes, secondes, où figurent 60; 360 et \num{3600}.
\item 1640 avant JC, le scribe égyptien Ahmes recopie le Papyrus Rhind. C'est le plus ancien document de mathématiques connu. Il se compose de problèmes résolus dans deux différentes branches de la discipline : l'arithmétique et la géométrie. Nous y trouvons la première approche de la constante $\pi \approx \num{3.160}$.
   \item Vers 520 avant JC, en Grèce, Pythagore définit la notion de nombres pairs et impairs. Il donne même un \textit{sexe} aux nombres.
   \item 628 après JC, Brahmagupta, mathématicien indien, définit dans un traité d'astronomie le zéro comme la soustraction d'un nombre par lui-même ($a - a = 0$). Il définit également qu'un nombre multiplié par zéro est égal à zéro.
   \item Vers 760 de notre ère, à Bagdad, les chiffres indiens sont repris par les arabes.
   \item En 825, Al-Khwarizmi, mathématicien persan à l'origine du mot \textit{algorithme}, écrit \textit{Al-jabr wa'l-muqâbalah}. Ce livre constitue une somme d'informations considérables sur la résolution d'équation de premier et de second degré. Les Occidentaux tireront le mot \textit{algèbre} d'Al-jabr. Il faut noter que cet ouvrage ne comprend aucun chiffre, tout est écrit littéralement !
   \item En 1202, Fibonacci, commerçant et un grand voyageur italien, originaire de Pise, publie le \textit{Liber abaci}, livre de calcul. Il va écumer la mer Méditerranée, et ses contacts avec les Arabes notamment, vont l'amener à utiliser leurs connaissances en mathématiques. Dans ce traité, il utilise pour la première fois les chiffres indo-arabes, qui vont peu à peu s'imposer face aux chiffres romains.
   \item En 1642, Pascal construit \textit{la Pascaline}, première machine à calculer mécanique. Objet que l'on peut toujours voir à Clermont-Ferrand.
   \item Vers 1700, Leibniz, soucieux d'éradiquer l'erreur humaine, remarque la facilité avec laquelle on fait les quatre opérations avec le système binaire, où tous les nombres sont représentés avec seulement 1 et 0. Ce système sera utile pour représenter le fonctionnement de l'électronique numérique utilisée aujourd'hui dans les ordinateurs et autres objets numériques.
   \item En 1872, le mathématicien allemand Dedekind énonce sa théorie des nombres, notamment sur les nombres réels, sur les nombres rationnels et sur les nombres irrationnels. Il ouvre la voie aux notations modernes des ensembles de nombres, que nous décrivons ci-dessous. Il collabora avec l'italien Peano et le Russe Cantor. Ce dernier prouva également que les nombres réels sont \og plus nombreux \fg{} que les entiers naturels et qu'il existe une \og infinité d'infini \fg{}.
\end{itemize}

Exemple d'écritures de l'année dans différents systèmes :

\begin{center}
% \huge = 25 pt
{\huge $\num{2024}$} en chiffres indo-arables, {\huge{\Romanbar{2024}}} en chiffres romains,

\includegraphics[scale=0.5]{Babylonian_2024.png} en mésopotamien et \includegraphics[scale=0.5]{Hiero_2024.png} en hiéroglyphe égyptien.
\end{center}


\newpage 
\section{Ensembles de nombres}
Le concept de nombre résiste à toute définition générale. Au fil de l'histoire, les mathématiciens ont progressivement pris conscience qu'il existait une infinité de nombres, de natures très variées. Ils ont alors \og rangés \fg{} en grandes familles les nombres ayant des propriétés identiques, de la même façon que les espèces sont classées en SVT, afin de d'en faciliter l'étude:\par
\begin{itemize}
\item L'ensemble $\mathbb{N}$ vient de l'appellation \textit{naturale} attribuée à Peano. Il désigne l'ensemble des nombres \textbf{positifs} appelés \textbf{entiers naturels}. Exemples : 0 ; 1 ; 2 ; 3 ; $2^8$ ; \num{1000}. Si l'on note \textbf{$\mathbb{N}^*$}, cela signifie que l'on \textbf{exclut le zéro}.
\item L'ensemble \textbf{$\mathbb{Z}$} vient de l'allemand \textit{zahlen} qui signifie \textit{compter}. Ainsi défini par Dedekind, il recouvre l'ensemble des nombres \textbf{positif ou négatif}, appelés \textbf{entiers relatifs}. Exemples : $-3$ ; $-1$ ; 0 ; 1 ; 5. Les nombres entiers naturels sont également des entiers relatifs, nous disons que $\mathbb{N}$ est \textbf{inclus dans} $\mathbb{Z}$. Ce qui se note $\mathbb{N} \subset \mathbb{Z}$.
   \item L'ensemble $\mathbb{D}$ est l'ensemble des \textbf{nombres décimaux}, qui peuvent s'écrire exactement avec un nombre fini de chiffres après la virgule. Ils peuvent donc également s'écrire $\frac{a}{10^n}$ avec $a \in \mathbb{Z}$ et $n \in \mathbb{N}$. Exemple : $\frac{3}{4}=\num{0.75}=\frac{75}{100}=\frac{75}{10^2}$. On a $\mathbb{Z}\subset\mathbb{D}$.
   \item L'ensemble $\mathbb{Q}$ a été défini par Peano, il vient de l'italien \textit{quotiente (fraction)}. Il définit l'ensemble des \textbf{nombres rationnels}. Ces nombres s'écrivent sous la forme $\frac{a}{b}$ avec $a\in\mathbb{Z}$ et $b\in\mathbb{N}^*$. Exemples : $-3$ ; $\num{-2.5}$ ; $0$ ; $\num{1.25}$ ; $\frac{1}{3}$ ; $\num{2.666}$. Le nombre peut être décimal ou périodique ($\frac{2}{3} =\num{0.666}\ldots $; $\frac{22}{7} =\num{3.142857142857}\ldots$). On a $\mathbb{D}\subset\mathbb{Q}$
   \item L'ensemble $\mathbb{R}$, défini par Dedekind, pour \textit{real}, recouvre les \textbf{nombres réels}. Exemples : $-1/3$ ; 0 ; $\pi$ ; $\sqrt{2}$. On a $\mathbb{Q}\subset\mathbb{R}$.
\end{itemize}

\textbf{A retenir :}\par
\smallskip % espace vertical de petite taille
\fbox{
\begin{minipage}{0.6\linewidth}
\bigskip % espace vertical de grande taille
%----- Diagramme Venn Ensembles ---------
% Dessin inspiré de l'original d'Arié Yallouz
\begin{center}
\begin{pspicture}(0,-.5)(10,6.5)
\psset{linewidth=1pt,framearc=0.5}
\psframe(1.2,1.2)(4,2.5)
\uput*[r](2,2.5){{\Large $\mathbb{N}$}} % \Large=14pt
\uput[u](1.5,2){$0$}
\uput[u](2,1.5){$1$}
\uput[d](2.9,1.9){$2^3$}
\uput[l](3.8,2){$123$}
\psframe(.9,.9)(5.5,3.5)
\uput*[r](2,3.5){\Large $\mathbb{Z}$}
\uput[r](3,2.9){$-1$}
\uput[d](4.5,3.2){$-360$}
\uput[d](4.6,2){$-3^4$}
\psframe(.6,.6)(7,4.5)
\uput*[r](2,4.5){\Large $\mathbb{D}$}
\uput[r](3,4){$\num{-0.475}$}
\uput[r](6,3.1){$\num{3.14}$}
\uput[d](6,4.3){$9\times10^{-3}$}
\uput[d](6.5,2.5){$-\dfrac{3}{4}$}
\psframe(.3,.3)(8.5,5.5)
\uput*[r](2,5.5){\Large $\mathbb{Q}$}
\uput[d](7.8,4.5){$\dfrac{22}{7}$}
\uput[d](7.4,2.5){$-\dfrac{4}{3}$}
\psframe[linewidth=1.25pt,framearc=0.5](0,0)(10,6.5)
\uput[d](3.7,6.5){$\sqrt{2}$}
\uput[d](9.2,5.2){$\pi$}
\uput[dl](9.7,2){$-\sqrt{\dfrac{3}{4}}$}
\uput*[r](2,6.5){\Large $\mathbb{R}$}
\end{pspicture}
\end{center}
%--------------------------
\end{minipage}
} % Fin Fbox
\hfill %remplit l'espace restant sur la ligne courante
\begin{minipage}{0.3\linewidth}
\begin{center}
  \includegraphics[width=4cm]{poupees_gigognes.png}
\end{center}
\end{minipage}
\par
\medskip % espace de taille moyenne

Remarques : 
\begin{itemize}
\item Cette classification se comporte comme des poupées gigognes, en  mathématique on dit : $\mathbb{N} \subset \mathbb{Z} \subset \mathbb{D} \subset \mathbb{Q} \subset \mathbb{R} \subset \mathbb{C}$.
\item Il existe d'autres ensembles, qui ne sont pas au programme de seconde. Par exemples, l'ensemble $\mathbb{C}$ pour les nombres complexes étudiés en option \textit{maths expertes} ; $\mathbb{H}$ pour les hypercomplexes ; $\mathbb{O}$ pour les Octavions ; etc.
\end{itemize}


\section{Ressources}
\begin{itemize}
   \item \href{https://www.seuil.com/ouvrage/mathematica-david-bessis/9782021493979}{Mathematica}, Une aventure au cœur de nous-mêmes, David Bessis, 2022, Edition Seuil	 
   \item \href{https://editions.flammarion.com/Catalogue/hors-collection/sciences/le-grand-roman-des-maths}{Le grand roman des maths}, De la préhistoire à nos jours, Mickaël Launay, 2016, Edition Flammarion
   \item L'extraordinaire aventure du chiffre 1 \url{https://youtu.be/kjxPNH8CYVA}
   \item Aux origines des nombres et du calcul \url{https://youtu.be/-FuzROk9KLQ}
   \item \url{https://www.lumni.fr/article/les-maths-les-ensembles-de-nombres}
   \item \url{https://fr.wikipedia.org/wiki/Num%C3%A9ration_%C3%A9gyptienne}
   \item \url{https://fr.wikipedia.org/wiki/Num%C3%A9ration_m%C3%A9sopotamienne}
\end{itemize}

\end{Maquette}



\end{document}


% Local Variables:
% TeX-engine: luatex
% End:
