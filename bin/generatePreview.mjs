/**
 * Récupère les thèmes à générer dans themes.json
 * Lit les fichiers .tex dans les répertoires des thèmes et sous-thèmes
 * et génère les fichiers enonce, solution, réponse, indice et correction dans output
 * le fichier png et _corr.png à la racine du thème
 
 */

import { readFileSync, writeFileSync, unlinkSync } from "node:fs"
import { join, basename } from "node:path"
import { execSync } from "node:child_process"
import { createRequire } from "node:module"

const require = createRequire(import.meta.url)
const readline = require("node:readline")
const fs = require("node:fs")
const path = require("node:path")

const rootDir = "./2nde"
const themes = require("./themes.json")

for (const theme in themes) {
  for (const sousTheme in themes[theme].sousThemes) {
    console.log(`Génération des fichiers pour le sous-thème ${sousTheme}`)
    let dirTexPath = path.join(rootDir, themes[theme].sousThemes[sousTheme].url)
    const dirPngPath = path.join(dirTexPath, "png")
    dirTexPath = path.join(dirTexPath, "tex")
    createCleanDirectory(dirPngPath)
    fs.readdir(dirTexPath, (err, files) => {
      if (err) {
        console.error(`Impossible de lire le répertoire ${dirTexPath}`)
        process.exit(1)
      }

      const texFiles = files.filter((file) => path.extname(file) === ".tex")

      for (const file of texFiles) {
        const filePath = path.join(dirTexPath, file)
        const content = readFileSync(filePath, "utf8")
        if (content.includes('% @Mathalea')) {
          continue
        }
        const outputDirPath = path.join(dirTexPath, "output")
        createCleanDirectory(outputDirPath)
        const enonceFilePath = join(dirTexPath, "output", file).replace(
          ".tex",
          "_enonce.tex"
        )
        const reponseFilePath = enonceFilePath.replace(
          "_enonce.tex",
          "_reponse.tex"
        )
        const solutionFilePath = enonceFilePath.replace(
          "_enonce.tex",
          "_solution.tex"
        )
        const indiceFilePath = enonceFilePath.replace(
          "_enonce.tex",
          "_indice.tex"
        )
        const regexEnonce =
          /\\begin\{exercice\}(?:\[[\s\S]*?\])?([\s\S]*?)\\end\{exercice\}/gm
        const enonce = regexEnonce.exec(content)
        if (enonce) {
          writeFileSync(enonceFilePath, enonce[1])
          const enonceTmpFilePath = enonceFilePath.replace(".tex", ".tmp.tex")
          writeFileSync(enonceTmpFilePath, addPreamble(enonce[1]))
          buildDocument(enonceTmpFilePath)
        }
        const regexCorrection = /\\begin\{Reponse\}([\s\S]*?)\\end\{Reponse\}/gm
        const correction = regexCorrection.exec(content)
        if (correction) {
          writeFileSync(reponseFilePath, correction[1])
          const correctionTmpFilePath = reponseFilePath.replace(
            ".tex",
            ".tmp.tex"
          )
          writeFileSync(correctionTmpFilePath, addPreamble(correction[1]))
          buildDocument(correctionTmpFilePath)
        }
        const regexSolution = /\\begin\{Solution\}([\s\S]*?)\\end\{Solution\}/gm
        const solution = regexSolution.exec(content)
        if (solution) {
          writeFileSync(solutionFilePath, solution[1])
          const solutionTmpFilePath = solutionFilePath.replace(
            ".tex",
            ".tmp.tex"
          )
          writeFileSync(solutionTmpFilePath, addPreamble(solution[1]))
          buildDocument(solutionTmpFilePath)
        }
        const regexIndice = /\\begin\{Indice\}([\s\S]*?)\\end\{Indice\}/gm
        const indice = regexIndice.exec(content)
        if (indice) {
          writeFileSync(indiceFilePath, indice[1])
          const indiceTmpFilePath = indiceFilePath.replace(".tex", ".tmp.tex")
          writeFileSync(indiceTmpFilePath, addPreamble(indice[1]))
          buildDocument(indiceTmpFilePath)
        }
      }
      const filesInDirPngPath = fs.readdirSync(dirPngPath)
      const tmpFiles = filesInDirPngPath.filter(
        (file) =>
          file.includes(".tmp.tex") ||
          file.includes(".tmp.aux") ||
          file.includes(".tmp.log") ||
          file.includes(".tmp.out") ||
          file.includes(".tmp.pdf")
      )
      for (const file of tmpFiles) {
        const filePath = path.join(dirPngPath, file)
        try {
          unlinkSync(filePath)
        } catch (error) {
          console.log(error)
        }
      }
    })
  }
}

function addPreamble(content) {
  return `\\documentclass[french,preview,border=1pt]{standalone}
\\usepackage[a4paper]{geometry}
\\usepackage{ProfCollege}
\\usepackage{amssymb}
\\usepackage{qrcode}
\\usepackage{multicol}
\\usepackage{tabularx}
\\usepackage[luatex]{hyperref}
\\begin{document}
${content}
\\end{document}
`
}

function buildDocument(filePath) {
  // Parent directory of filePath
  const dirPngPath = path.dirname(filePath).replace("tex/output", "png")
  const pdfPath = filePath.replace("tex/output", "png").replace(".tex", ".pdf")
  try {
    execSync(
      `lualatex -output-directory=${dirPngPath} -interaction=nonstopmode -file-line-error -halt-on-error ${filePath}  `
    )
  } catch (error) {
    console.warn(`Erreur dans la compilation de ${filePath}...`)
  } finally {
    console.log(`Compilation de ${filePath} terminée`)
  }
  try {
    execSync(
      `magick -density 300 -background white -flatten ${pdfPath} -quality 90 ${pdfPath.replace(
        ".tmp.pdf",
        ".png"
      )}`
    )
  } catch (error) {
    console.log(`Erreur de conversion pour ${pdfPath}`)
  }
}

function createCleanDirectory(dirPath) {
  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath)
  } else {
    const filesInDirPath = fs.readdirSync(dirPath)
    for (const file of filesInDirPath) {
      const filePath = path.join(dirPath, file)
      try {
        unlinkSync(filePath)
      } catch (error) {
        console.log(`Impossible de nettoyer le répertoire ${dirPath}`)
      } finally {
        console.log(`Nettoyage du répertoire ${dirPath} terminé`)
      }
    }
  }
}
