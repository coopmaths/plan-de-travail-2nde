import { createRequire } from "node:module"

const require = createRequire(import.meta.url)
const readline = require("node:readline")
const fs = require("node:fs")
const path = require("node:path")

const rootDir = "./2nde"
const outputDir = "static/2nd/"
const themes = require("./themes.json")

const referentiel = { '2nde': {} }

for (const theme in themes) {
  if (referentiel["2nde"][theme] === undefined) {
    referentiel["2nde"][theme] = {}
  }
  for (const sousTheme in themes[theme].sousThemes) {
    if (referentiel["2nde"][theme][sousTheme] === undefined) {
      referentiel["2nde"][theme][sousTheme] = {}
    }
    let dirPath = path.join(rootDir, themes[theme].sousThemes[sousTheme].url)
    dirPath = path.join(dirPath, "tex")
    await fs.readdir(dirPath, (err, files) => {
      if (err) {
        console.error(`Impossible de lire le répertoire ${dirPath}`)
      }
      const texFiles = files.filter((file) => path.extname(file) === ".tex")
      const infosForAsousTheme = []
      for (const file of texFiles) {
        const infosForOneFile = {}
        const content = fs.readFileSync(path.join(dirPath, file), "utf8")
        if (content.includes('% @Mathalea')) {
          continue
        }
        const regexDifficulte = /% @Difficulté (\d+)/
        const matchesDifficulte = content.match(regexDifficulte)
        if (matchesDifficulte) {
          infosForOneFile.difficulte = matchesDifficulte[1]
        }
        const regexTitre = /% @Titre (.*)/
        const matchesTitre = content.match(regexTitre)
        if (matchesTitre) {
          infosForOneFile.titre = matchesTitre[1]
        }
        const uuid = `2nd_${path.basename(file, ".tex")}`
        infosForOneFile.uuid = uuid
        const pngPath = dirPath.replace("/tex", "/png")
        const filePngEnonce = path.join(
          pngPath,
          file.replace(".tex", "_enonce.png")
        )
        const filePngReponse = path.join(
          pngPath,
          file.replace(".tex", "_reponse.png")
        )
        const filePngIndice = path.join(
          pngPath,
          file.replace(".tex", "_indice.png")
        )
        const filePngSolution = path.join(
          pngPath,
          file.replace(".tex", "_solution.png")
        )
        if (fs.existsSync(filePngEnonce)) {
          infosForOneFile.png = "static/" + filePngEnonce
          infosForOneFile.tex =
            "static/" +
            filePngEnonce.replace("/png/", "/tex/").replace(".png", ".tex")
        }
        if (fs.existsSync(filePngReponse)) {
          infosForOneFile.texReponse =
            "static/" +
            filePngReponse.replace("/png/", "/tex/").replace(".png", ".tex")
          infosForOneFile.pngReponse = "static/" + filePngReponse
        }
        if (fs.existsSync(filePngIndice)) {
          infosForOneFile.texIndice =
            "static/" +
            filePngIndice.replace("/png/", "/tex/").replace(".png", ".tex")
          infosForOneFile.pngIndice = "static/" + filePngIndice
        }
        if (fs.existsSync(filePngSolution)) {
          infosForOneFile.texCor =
            "static/" +
            filePngSolution.replace("/png/", "/tex/").replace(".png", ".tex")
          infosForOneFile.pngCor = "static/" + filePngSolution
        }
        infosForOneFile.typeExercice = "static"
        infosForAsousTheme.push(infosForOneFile)
      }
      infosForAsousTheme.sort((a, b) => a.difficulte - b.difficulte)
      for (const file of infosForAsousTheme) {
        referentiel["2nde"][theme][sousTheme][file.uuid] = file
      }
    })
  }
}

// Wait 4s
await new Promise((resolve) => setTimeout(resolve, 1000))
fs.writeFile(
  "./bin/referentiel.json",
  JSON.stringify(referentiel, null, 2),
  "utf8",
  (error) => {
    if (error) {
      console.error(`Erreur: ${error}`)
      return
    }
    console.log("Référentiel mis à jour")
  }
)
