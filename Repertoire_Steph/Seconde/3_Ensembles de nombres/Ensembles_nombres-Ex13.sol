Le périmètre du cercle est rationnel.\\
Notons $P$ ce périmètre et raisonnons par l'absurde.\\
Pour cela supposons que le le diamètre $d$ est rationnel. On a $p=\pi \times d$. Donc $\pi=\frac{p}{d}$ or $p$ et $d$ sont rationnels.\\
Donc $d$ est rationnel. Donc $\pi$ est rationnel, ce qui est absurde.\\
Donc le diamètre du cercle ne peut pas être rationnel. Donc le diamètre du cercle est irrationnel.
