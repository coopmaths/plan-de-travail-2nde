\begin{description}
\item $\dfrac{1}{2}=0,5 \in \mathbb{D}$. De plus $0<\dfrac{1}{2}<1$ donc $\dfrac{1}{2} \notin \mathbb{Z}$\\
\item $\sqrt{5} \in \mathbb{R}$. De plus $\sqrt{5} \notin \mathbb{Q}$ (cf démonstration racine 2).\\
\item $\dfrac{10-4}{3}=2 \in \mathbb{N}$\\
\item $-\sqrt{16}=-4 \in \mathbb{Z}$. De plus $-4<0$ donc $-4 \notin \mathbb{N}$
\item $\dfrac{1}{2}+\dfrac{1}{3}+\dfrac{1}{6}=\dfrac{3}{6}+\dfrac{2}{6}+\dfrac{1}{6}=1 \in \mathbb{N}$\\
\item$\sqrt{16}-\sqrt{25}=4-5=-1 \in \mathbb{Z}$. De plus $-1 \notin \mathbb{N}$\\
\item $\dfrac{91}{7}=13 \in \mathbb{N}$\\
\item$\dfrac{34}{2}-\sqrt{289}=0 \in \mathbb{N}$
\end{description}
