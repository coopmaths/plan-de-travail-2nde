1. $-5=\dfrac{-5}{10^0}$ est décimal.\\
2. $\dfrac{5}{7}$ n'est pas décimal. \\
Raisonnons par l'absurde et supposons qu'il existe $a \in \mathbb{N}$ et $n \in \mathbb{N}$ tels que $\dfrac{5}{7}=\dfrac{a}{10^n}$. \\
On a alors : $5 \times 10^n = a \times 7$. \\
Comme $7 a$ est un multiple de $7$ alors $5 \times 10^n$ est aussi un multiple de 7, ce qui est absurde car 7 est un nombre premier et ni 5 ni $10^n$ ne sont de multiples de 7 . \\
Donc $\dfrac{5}{7}$ n'est pas un nombre décimal.
3. $\dfrac{3}{40}=\dfrac{75}{1000}$ donc $\dfrac{3}{40}$ est un nombre décimal.\\
4. $\dfrac{40}{3}$ n'est pas décimal. \\Raisonnons par l'absurde et supposons qu'il existe $a \in \mathbb{N}$ et $n \in \mathbb{N}$ tel que $\dfrac{40}{3}=\dfrac{a}{10^n}$. \\
On a alors $: 40 \times 10^n=a \times 3$\\
$3 a$ est un multiple de 3 donc $40 \times 10^n$ aussi.\\
$40 \times 10^n$ est un multiple de 3 or la somme des chiffres de $40 \times 10^n$ est 4 donc ce n'est multiple de 3 .\\
On a donc une contradiction. Donc $\dfrac{40}{3}$ n'est pas un nombre décimal.
