On développe \\$\left(\sqrt{\dfrac{a}{c}}+\sqrt{\dfrac{c}{a}}\right)^2=\left(\sqrt{\dfrac{a}{c}}+\sqrt{\dfrac{c}{a}}\right)\left(\sqrt{\dfrac{a}{c}}+\sqrt{\dfrac{c}{a}}\right)$ sans oublier que le produit de racines carrées est égal à la racine carrée du produit.


